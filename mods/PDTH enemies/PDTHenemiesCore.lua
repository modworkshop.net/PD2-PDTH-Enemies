if not ModCore then
	log("[ERROR] Unable to find ModCore from BeardLib! Is BeardLib installed correctly?")
	return
end

PDTHEnemiesCore = PDTHEnemiesCore or class(ModCore)
function PDTHEnemiesCore:init()
	self.ai_groups = {
		"america",
		"russia",
		"pdth"
	}
	self.super.init(self, ModPath .. "main_config.xml", true, true)
end

if not _G.pdth_enemies then
	local success, err = pcall(function() PDTHEnemiesCore:new() end)
	if not success then
		log("[ERROR] An error occured on the initialization of PD:TH Enemies. " .. tostring(err))
	end
end
