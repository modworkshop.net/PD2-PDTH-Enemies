local enemy_conv = {
		["units/payday2/characters/ene_cop_1/ene_cop_1"] = "units/characters/enemies/cop/cop",
		["units/payday2/characters/ene_cop_2/ene_cop_2"] = "units/characters/enemies/cop2/cop2",
		["units/payday2/characters/ene_cop_3/ene_cop_3"] = "units/characters/enemies/cop3/cop3",
		["units/payday2/characters/ene_cop_4/ene_cop_4"] = "units/characters/enemies/cop3/cop3",
		["units/payday2/characters/ene_murkywater_1/ene_murkywater_1"] = "units/characters/enemies/murky_water1/murky_water1",
		["units/payday2/characters/ene_murkywater_2/ene_murkywater_2"] = "units/characters/enemies/murky_water2/murky_water2",
		["units/payday2/characters/ene_shield_1/ene_shield_1"] = "units/characters/enemies/shield/shield",
		["units/payday2/characters/ene_shield_2/ene_shield_2"] = "units/characters/enemies/shield/shield",
		["units/payday2/characters/ene_sniper_1/ene_sniper_1"] = "units/characters/enemies/sniper/sniper",
		["units/payday2/characters/ene_sniper_2/ene_sniper_2"] = "units/characters/enemies/sniper/sniper",
		["units/payday2/characters/ene_spook_1/ene_spook_1"] = "units/characters/enemies/spooc/spooc",
		["units/payday2/characters/bulldozer_1/bulldozer_1"] = "units/characters/enemies/tank/tank",
		["units/payday2/characters/ene_tazer_1/ene_tazer_1"] = "units/characters/enemies/taser/taser",
		["units/payday2/characters/ene_swat_1/ene_swat_1"] = "units/characters/enemies/swat/swat",
		["units/payday2/characters/ene_swat_2/ene_swat_2"] = "units/characters/enemies/swat_kevlar1/swat_kevlar1",
		["units/payday2/characters/ene_swat_heavy_1/ene_swat_heavy_1"] = "units/characters/enemies/swat_kevlar2/swat_kevlar2",
		["units/payday2/characters/ene_fbi_swat_1/ene_fbi_swat_1"] = "units/characters/enemies/swat2/swat2",
		["units/payday2/characters/ene_fbi_swat_2/ene_fbi_swat_2"] = "units/characters/enemies/swat3/swat3",
		["units/payday2/characters/ene_city_swat_1/ene_city_swat_1"] = "units/characters/enemies/swat_kevlar2/swat_kevlar2",
		["units/payday2/characters/ene_security_1/ene_security_1"] = "units/characters/enemies/security/security_guard_01",
		["units/payday2/characters/ene_security_2/ene_security_2"] = "units/characters/enemies/security/security_guard_01",
		["units/payday2/characters/ene_security_3/ene_security_3"] = "units/characters/enemies/security/security_guard_01",
		["units/payday2/characters/ene_security_4/ene_security_4"] = "units/characters/enemies/security/security_guard_01",
		["units/payday2/characters/ene_security_5/ene_security_5"] = "units/characters/enemies/security/security_guard_02",
		["units/payday2/characters/ene_security_5/ene_security_6"] = "units/characters/enemies/security/security_guard_02",
		["units/payday2/characters/ene_security_5/ene_security_7"] = "units/characters/enemies/security/security_guard_02"
	}

function ElementSpawnEnemyDummy:init(...)
	ElementSpawnEnemyDummy.super.init(self, ...)
	local ai_type = tweak_data.levels:get_ai_group_type()
	if ai_type == "pdth" then
		if enemy_conv[self._values.enemy] then
			self._values.enemy = enemy_conv[self._values.enemy]
		end
		self._values.enemy = enemy_conv[self._values.enemy] or self._values.enemy
	end
	self._enemy_name = self._values.enemy and Idstring(self._values.enemy) or Idstring("units/payday2/characters/ene_swat_1/ene_swat_1")
	self._values.enemy = nil
	self._units = {}
	self._events = {}
	self:_finalize_values()
end
