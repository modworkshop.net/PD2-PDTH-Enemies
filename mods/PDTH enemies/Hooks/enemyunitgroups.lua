EnemyUnitGroups[LevelsTweakData.LevelType.pdth] = function()
	local group = {
		Idstring("units/characters/enemies/spooc/spooc"),
		Idstring("units/characters/enemies/swat_kevlar1/swat_kevlar1"),
		Idstring("units/characters/enemies/swat/swat"),
		Idstring("units/characters/enemies/swat2/swat2"),
		Idstring("units/characters/enemies/swat3/swat3"),
		Idstring("units/characters/enemies/swat_kevlar2/swat_kevlar2"),
		Idstring("units/characters/enemies/taser/taser"),
		Idstring("units/characters/enemies/shield/shield"),
		Idstring("units/characters/enemies/tank/tank"),
		Idstring("units/characters/enemies/cop/cop"),
		Idstring("units/characters/enemies/cop2/cop2"),
		Idstring("units/characters/enemies/cop3/cop3"),
		Idstring("units/characters/enemies/fbi1/fbi1"),
		Idstring("units/characters/enemies/fbi2/fbi2"),
		Idstring("units/characters/enemies/fbi3/fbi3"),
		Idstring("units/characters/enemies/sniper/sniper"),
		Idstring("units/characters/enemies/security/security_guard_01"),
		Idstring("units/characters/enemies/security/security_guard_02"),
		Idstring("units/pd2_dlc_vip/characters/ene_phalanx_1/ene_phalanx_1"),
		Idstring("units/pd2_dlc_vip/characters/ene_vip_1/ene_vip_1")
	}
	return group
end
