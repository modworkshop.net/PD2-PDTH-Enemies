function CopDamage:_spawn_head_gadget(params)
	if not self._head_gear then
		return
	end
	local pdth = LevelsTweakData.LevelType.PDTH
	local ai_type = tweak_data.levels:get_ai_group_type()
	if self._head_gear_object then
		if self._nr_head_gear_objects then
			for i = 1, self._nr_head_gear_objects do
				local head_gear_obj_name = self._head_gear_object .. tostring(i)
				if ai_type == "pdth" then
					head_gear_obj_name = self._head_gear_object
				end
				self._unit:get_object(Idstring(head_gear_obj_name)):set_visibility(false)
			end
		else
			self._unit:get_object(Idstring(self._head_gear_object)):set_visibility(false)
		end
		if self._head_gear_decal_mesh then
			local mesh_name_idstr = Idstring(self._head_gear_decal_mesh)
			self._unit:decal_surface(mesh_name_idstr):set_mesh_material(mesh_name_idstr, Idstring("flesh"))
		end
	end
	local unit = World:spawn_unit(Idstring(self._head_gear), params.position, params.rotation)
	if not params.skip_push then
		local dir = math.UP - params.dir / 2
		dir = dir:spread(25)
		local body = unit:body(0)
		body:push_at(body:mass(), dir * math.lerp(300, 650, math.random()), unit:position() + Vector3(math.rand(1), math.rand(1), math.rand(1)))
	end
	self._head_gear = false
end
