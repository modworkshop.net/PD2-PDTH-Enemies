GroupAITweakData.pdth_init_unit_categories = GroupAITweakData._init_unit_categories

function GroupAITweakData:_init_unit_categories(...)
	
		GroupAITweakData:pdth_init_unit_categories(...)
		
		local spooc = {
				Idstring("units/characters/enemies/spooc/spooc")
			}
		
		local kevlar1 = {
				Idstring("units/characters/enemies/swat_kevlar1/swat_kevlar1")
			}
			
		local swat = {
				Idstring("units/characters/enemies/swat/swat")
			}
			
		local swat2 = {
				Idstring("units/characters/enemies/swat2/swat2")
			}
			
		local swat3 = {
				Idstring("units/characters/enemies/swat3/swat3")
			}
		
		local kevlar2 = {
				Idstring("units/characters/enemies/swat_kevlar2/swat_kevlar2")
			}
			
		local taser = {
				Idstring("units/characters/enemies/taser/taser")
			}
			
		local shield = {
				Idstring("units/characters/enemies/shield/shield")
			}
			
		local tank = {
				Idstring("units/characters/enemies/tank/tank")
			}
		
		local phalanx = {
				Idstring("units/pd2_dlc_vip/characters/ene_phalanx_1/ene_phalanx_1")
			}
			
		local vip = {
				Idstring("units/pd2_dlc_vip/characters/ene_vip_1/ene_vip_1")
			}
			
		local fbi1 = {
				Idstring("units/characters/enemies/fbi1/fbi1")
			}
			
		local fbi2 = {
				Idstring("units/characters/enemies/fbi3/fbi3")
			}
			
		local fbi3 = {
				Idstring("units/characters/enemies/fbi3/fbi3")
			}
		
		self.unit_categories.spooc.unit_types.pdth = spooc
		
		self.unit_categories.CS_cop_C45_R870.unit_types.pdth = swat3
		self.unit_categories.CS_swat_R870.unit_types.pdth = swat3
		self.unit_categories.FBI_swat_R870.unit_types.pdth = kevlar1
		
		self.unit_categories.CS_cop_stealth_MP5.unit_types.pdth = swat
		self.unit_categories.CS_swat_MP5.unit_types.pdth = swat
		self.unit_categories.FBI_suit_stealth_MP5.unit_types.pdth = fbi3
		
		self.unit_categories.CS_heavy_M4.unit_types.pdth = swat2
		self.unit_categories.CS_heavy_M4_w.unit_types.pdth = kevlar2
		self.unit_categories.FBI_suit_C45_M4.unit_types.pdth = fbi1
		self.unit_categories.FBI_suit_M4_MP5.unit_types.pdth = fbi3
		self.unit_categories.FBI_swat_M4.unit_types.pdth = kevlar2
		self.unit_categories.FBI_heavy_G36_w.unit_types.pdth = kevlar2
		self.unit_categories.FBI_heavy_G36.unit_types.pdth = kevlar2
		
		self.unit_categories.CS_tazer.unit_types.pdth = taser
		
		self.unit_categories.CS_shield.unit_types.pdth = shield
		self.unit_categories.FBI_shield.unit_types.pdth = shield
		
		self.unit_categories.FBI_tank.unit_types.pdth = tank
		
		self.unit_categories.Phalanx_minion.unit_types.pdth = phalanx
		
		self.unit_categories.Phalanx_vip.unit_types.pdth = vip

end